package asensio.cristina.androidchallenge.connection

import android.content.Context
import android.net.ConnectivityManager

interface Connectivity {

    fun isDeviceConnectedToWifiOrData(context: Context): Pair<Boolean, String>
}