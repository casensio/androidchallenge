package asensio.cristina.androidchallenge.connection

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import asensio.cristina.androidchallenge.R

class ConnectivityImpl: Connectivity {

    override fun isDeviceConnectedToWifiOrData(context: Context): Pair<Boolean, String> {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return Pair(netInfo?.isConnected ?: false, netInfo?.reason
                ?: context.resources.getString(R.string.no_internet))
    }
}