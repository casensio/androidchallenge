package asensio.cristina.androidchallenge.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import asensio.cristina.androidchallenge.R


class NoInternetFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_internet, container, false)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnNoInternetFragmentInteractionListener) {
        } else {
            throw RuntimeException(context.toString() + " must implement OnNoInternetFragmentInteractionListener")
        }
    }

    interface OnNoInternetFragmentInteractionListener {
        fun onNoInternetFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                NoInternetFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}
