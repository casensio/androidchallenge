package asensio.cristina.androidchallenge.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import asensio.cristina.androidchallenge.R
import asensio.cristina.androidchallenge.ShowItem
import asensio.cristina.androidchallenge.adapter.ShowAdapter
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_page.view.*

private const val SHOW_PARAM = "show"

class PageFragment : Fragment() {
    private var show: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            show = it.getString(SHOW_PARAM)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val gson = Gson()
        val showItem = gson.fromJson(show, ShowItem::class.java)

        displayDataInLayout(view, showItem)
    }

    private fun displayDataInLayout(view: View, showItem: ShowItem) {
        view.detailShowTitle.text = showItem.name
        var overview = showItem.overview

        if (overview.length > 250) {
            overview = overview.substring(0, 250) + "..."
        }

        view.detailShowOverview.text = overview
        view.detailShowOverview.setOnClickListener { v ->
            Toast.makeText(context, showItem.overview, Toast.LENGTH_LONG).show()
        }

        Glide.with(context)
                .load(ShowAdapter.URL_SHOWS_IMAGES + showItem.poster_path)
                .error(R.drawable.logo)
                .into(view.detailShowImage)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(position: Int, show: String) =
                PageFragment().apply {
                    arguments = Bundle().apply {
                        putString(SHOW_PARAM, show)
                    }
                }
    }
}
