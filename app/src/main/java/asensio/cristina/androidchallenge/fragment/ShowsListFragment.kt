package asensio.cristina.androidchallenge.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import asensio.cristina.androidchallenge.R
import asensio.cristina.androidchallenge.ShowItem
import asensio.cristina.androidchallenge.adapter.ShowAdapter
import com.google.gson.Gson
import org.json.JSONArray

private const val RESULTS_PARAM = "results"

class ShowsListFragment : Fragment() {
    private var shows: MutableList<ShowItem> = mutableListOf<ShowItem>()
    private var results: String? = null
    private var listener: OnShowsListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            results = it.getString(RESULTS_PARAM)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_shows_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                val gson = Gson()
                val resultsJsonArray = JSONArray(results)

                for (i in 0 until resultsJsonArray.length()) {
                    val showObject = resultsJsonArray.getJSONObject(i)
                    val show = gson.fromJson(showObject.toString(), ShowItem::class.java)
                    shows.add(show)
                }

                layoutManager = android.support.v7.widget.LinearLayoutManager(context)
                adapter = ShowAdapter(context, shows)
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnShowsListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnShowsListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnShowsListFragmentInteractionListener {
        fun onShowsListFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(results: String) =
                ShowsListFragment().apply {
                    arguments = Bundle().apply {
                        putString(RESULTS_PARAM, results)
                    }
                }
    }
}
