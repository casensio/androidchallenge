package asensio.cristina.androidchallenge.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import asensio.cristina.androidchallenge.R
import asensio.cristina.androidchallenge.ShowItem
import asensio.cristina.androidchallenge.activity.ShowDetailActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.show_row.view.*

class ShowAdapter(
        private val context: Context,
        private val shows: List<ShowItem>)
    : RecyclerView.Adapter<ShowAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.show_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shows.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val show = shows[position]
        holder.showTitle.text = show.name
        holder.showScore.rating = show.popularity
        Glide.with(context)
                .load(URL_SHOWS_IMAGES + show.poster_path)
                .error(R.drawable.logo)
                .into(holder.showImage)

        holder.itemView.setOnClickListener {
            val intent = Intent(context, ShowDetailActivity::class.java)
            intent.putExtra("showId", show.id)
            context.startActivity(intent)
        }
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val showImage: ImageView = mView.cardviewShowImage
        val showTitle: TextView = mView.cardviewShowTitle
        val showScore: RatingBar = mView.cardviewShowScore

    }

    companion object {
        const val URL_SHOWS_IMAGES = "http://image.tmdb.org/t/p/w500/"
    }
}
