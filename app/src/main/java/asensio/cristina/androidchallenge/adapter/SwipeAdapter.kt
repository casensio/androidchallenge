package asensio.cristina.androidchallenge.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import asensio.cristina.androidchallenge.fragment.PageFragment
import org.json.JSONArray

class SwipeAdapter(
        fragmentManager: FragmentManager,
        private val items: String)
    : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        val resultsJsonArray = JSONArray(items)
        val show = resultsJsonArray.get(position + 1).toString()
        return PageFragment.newInstance(position + 1, show)
    }

    override fun getCount(): Int {
        val resultsJsonArray = JSONArray(items)
        return resultsJsonArray.length()
    }

}