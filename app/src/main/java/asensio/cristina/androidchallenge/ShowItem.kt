package asensio.cristina.androidchallenge

data class ShowItem(
        val id: Int,
        val name: String,
        val popularity: Float,
        val poster_path: String,
        val overview: String
        )