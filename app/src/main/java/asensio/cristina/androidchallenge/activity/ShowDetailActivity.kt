package asensio.cristina.androidchallenge.activity

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.widget.Toast
import asensio.cristina.androidchallenge.R
import asensio.cristina.androidchallenge.adapter.SwipeAdapter
import asensio.cristina.androidchallenge.connection.Connectivity
import asensio.cristina.androidchallenge.connection.ConnectivityImpl
import asensio.cristina.androidchallenge.fragment.PageFragment
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_show_detail.*
import org.json.JSONObject

private const val SHOWS_URL = "https://api.themoviedb.org/3/tv/"
private const val SIMILAR_SHOWS = "/similar?"
private const val API_KEY = "97b1f6ef8fd13f3a02c59dd498f2cd9d"
private const val API_KEY_PARAM = "api_key="
private const val JSON_COLLECTION_NAME = "results"

class ShowDetailActivity : FragmentActivity(), PageFragment.OnFragmentInteractionListener {
    private val connectivity: Connectivity = ConnectivityImpl()
    private var requestQueue: RequestQueue? = null
    private val noShowId = -1
    private val TAG = ShowDetailActivity::class.java.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_detail)

        val actionBar = getActionBar()
        actionBar?.hide()

        val showId = intent.getIntExtra("showId", noShowId)
        val (connected, reason) = connectivity.isDeviceConnectedToWifiOrData(applicationContext)

        if (connected && showId != noShowId) {

            requestQueue = Volley.newRequestQueue(this)

            downloadSimilarShows(showId)

        } else {
            Toast.makeText(applicationContext, "$reason", Toast.LENGTH_SHORT).show()
        }
    }

    private fun downloadSimilarShows(showId: Int) {
        val similarShowsRequest = JsonObjectRequest(Request.Method.GET, SHOWS_URL + showId + SIMILAR_SHOWS + API_KEY_PARAM + API_KEY, null,
                Response.Listener { response ->
                    Log.d(TAG, "Similar Shows Response: %s".format(response.toString()))

                    val json = JSONObject(response.toString())
                    val items = json.getJSONArray(JSON_COLLECTION_NAME).toString()
                    val swipeAdapter = SwipeAdapter(supportFragmentManager, items)

                    viewPager.adapter = swipeAdapter
                },

                Response.ErrorListener { error ->
                    Log.e(TAG, error.message)
                })

        similarShowsRequest.tag = TAG

        requestQueue?.add(similarShowsRequest)
    }

    override fun onFragmentInteraction(uri: Uri) {

    }
}

