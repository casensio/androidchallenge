package asensio.cristina.androidchallenge.activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import asensio.cristina.androidchallenge.R
import asensio.cristina.androidchallenge.connection.Connectivity
import asensio.cristina.androidchallenge.connection.ConnectivityImpl
import asensio.cristina.androidchallenge.fragment.NoInternetFragment
import asensio.cristina.androidchallenge.fragment.ShowsListFragment
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

private const val POPULAR_SHOWS_URL = "https://api.themoviedb.org/3/tv/popular?"
private const val API_KEY = "97b1f6ef8fd13f3a02c59dd498f2cd9d"
private const val API_KEY_PARAM = "api_key="
private const val JSON_COLLECTION_NAME = "results"
private val TAG = MainActivity::class.java.name

class MainActivity :
        AppCompatActivity(),
        ShowsListFragment.OnShowsListFragmentInteractionListener,
        NoInternetFragment.OnNoInternetFragmentInteractionListener {

    private var requestQueue: RequestQueue? = null
    private val connectivity : Connectivity = ConnectivityImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val actionBar = getSupportActionBar()
        actionBar?.hide()

        val (connected, reason) = connectivity.isDeviceConnectedToWifiOrData(applicationContext)

        if (connected) {
            requestQueue = Volley.newRequestQueue(this)
            downloadPopularShows()
        } else {
            setNoInternetFragment()
            Toast.makeText(applicationContext, "$reason", Toast.LENGTH_SHORT).show()
        }

    }

    private fun downloadPopularShows() {
        val popularShowsRequest = JsonObjectRequest(Request.Method.GET, POPULAR_SHOWS_URL + API_KEY_PARAM + API_KEY, null,

                Response.Listener { response ->
                    Log.d(TAG, "Popular Shows Response: %s".format(response.toString()))
                    val json = JSONObject(response.toString())
                    val items = json.getJSONArray(JSON_COLLECTION_NAME).toString()
                    setShowsListFragment(items)
                },

                Response.ErrorListener { error ->
                    Log.e(TAG, error.message)
                })

        popularShowsRequest.tag = TAG

        requestQueue?.add(popularShowsRequest)
    }

    private fun setNoInternetFragment(): Unit {
        val fragment = NoInternetFragment.newInstance()
        switchFragment(fragment)
    }

    private fun setShowsListFragment(items: String): Unit {
        val fragment = ShowsListFragment.newInstance(items)
        switchFragment(fragment)
    }

    private fun switchFragment(fragment: Fragment): Unit {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentFragment, fragment)
        transaction.commit()
    }

    override fun onShowsListFragmentInteraction(uri: Uri) {

    }

    override fun onNoInternetFragmentInteraction(uri: Uri) {

    }

    companion object {

    }
}
